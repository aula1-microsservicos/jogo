package com.itau.jogo.controller;

import java.util.List;

import javax.jms.ObjectMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.jogo.amq.AmqSender;
import com.itau.jogo.kafka.ProducerKafka;
import com.itau.jogo.log.Events;
import com.itau.jogo.model.Jogo;
import com.itau.jogo.model.Pergunta;
import com.itau.jogo.model.Ranking;
import com.itau.jogo.model.Resposta;

@RestController
public class JogoController {
	
	@Autowired AmqSender amqSender;
	//@Autowired ProducerKafka producerKafka;

	@RequestMapping(method = RequestMethod.GET, path = "/iniciar")
	public Jogo iniciarJogo() {

		RestTemplate restTemplate = new RestTemplate(); 
		
		Jogo jogo = new Jogo();
		jogo.setNomeJogo("Teste1");

		String url = "http://10.162.108.180:9000/pergunta/obtemperguntas/10";

		try {
			 ResponseEntity<List<Pergunta>> resposta = 
					 restTemplate.exchange(url,HttpMethod.GET, null, 
							 new ParameterizedTypeReference<List<Pergunta>>() {});
			
			jogo.setQuestoes(resposta.getBody());
			jogo.setIniciaok(true);
		} catch (Exception e) {
			jogo.setIniciaok(false);
		}

		return jogo;

	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/iniciarjogo")
	public Jogo iniciaJogo() {
		
		Jogo jogo = new Jogo();
		jogo.setNomeJogo("Teste1");

		//ProducerKafka.gravarNoLog("Jogo a iniciar");
		try {
			ObjectMessage msg = amqSender.obtemPergunta(10, "yes");
			List<Pergunta> listaPerg = (List<Pergunta>) msg.getObject();
			jogo.setQuestoes( listaPerg);
			jogo.setIniciaok(true);
		} catch (Exception e) {
			jogo.setIniciaok(false);
			
		}
		//ProducerKafka.gravarNoLog("Jogo iniciado");
		Events.emmitKafka(jogo.getTrackedType(), "created", jogo);
		return jogo;

	}

	@RequestMapping(method = RequestMethod.POST, path = "/finalizar")
	public void finalizarJogo(@RequestBody Resposta resposta) {

		RestTemplate restTemplate = new RestTemplate();

		Ranking ranking = new Ranking();

		ranking.setNomeJogador(resposta.getNomeJogador());
		ranking.setNomeJogo(resposta.getNomeJogo());
		ranking.setPerguntasCertas(resposta.getRespostasCertas());
		ranking.setPerguntasErradas(resposta.getRespostasErradas());

		String url = "http://10.162.107.12:8080/incluirRanking";

		restTemplate.postForObject(url, ranking, Ranking.class);
	}

}
