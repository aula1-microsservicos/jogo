package com.itau.jogo.model;

public class Ranking {

	private long id;
	private String nomeJogo;
	private String tipoJogo;
	private String nomeJogador;
	private int perguntasCertas;
	private int perguntasErradas;
	private int pontuacao;

	public long getId() {
		return id;
	}
	

	public void setId(long id) {
		this.id = id;
	}

	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public String getTipoJogo() {
		return tipoJogo;
	}

	public void setTipoJogo(String tipoJogo) {
		this.tipoJogo = tipoJogo;
	}

	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	public int getPerguntasCertas() {
		return perguntasCertas;
	}

	public void setPerguntasCertas(int perguntasCertas) {
		this.perguntasCertas = perguntasCertas;
	}

	public int getPerguntasErradas() {
		return perguntasErradas;
	}

	public void setPerguntasErradas(int perguntasErradas) {
		this.perguntasErradas = perguntasErradas;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

}
