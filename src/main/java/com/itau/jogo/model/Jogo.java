package com.itau.jogo.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Jogo implements Trackable{

	private String nomeJogo;
	private boolean iniciaok;
	private List<Pergunta> questoes;
	
	

	public boolean isIniciaok() {
		return iniciaok;
	}

	public void setIniciaok(boolean iniciaok) {
		this.iniciaok = iniciaok;
	}

	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public List<Pergunta> getQuestoes() {
		return questoes;
	}

	public void setQuestoes(List<Pergunta> list) {
		this.questoes = list;
	}

	@Override
	public String getTrackedType() {
		return "jogo";
	}

	@Override
	public Map<String, String> getTrackedProperties() {
		Map<String, String> propriedades = new HashMap<String, String>();
		propriedades.put("iniciaok", String.valueOf(iniciaok));
		propriedades.put("nomeJogo", String.valueOf(nomeJogo));
		propriedades.put("quantidadePerguntas", String.valueOf(questoes.size()));
		return propriedades;
	}

}
