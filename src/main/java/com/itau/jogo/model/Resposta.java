package com.itau.jogo.model;

public class Resposta {

	private String nomeJogador;
	private String nomeJogo;
	private int respostasCertas;
	private int respostasErradas;

	public String getNomeJogador() {
		return nomeJogador;
	}

	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}

	
	public String getNomeJogo() {
		return nomeJogo;
	}

	public void setNomeJogo(String nomeJogo) {
		this.nomeJogo = nomeJogo;
	}

	public int getRespostasCertas() {
		return respostasCertas;
	}

	public void setRespostasCertas(int respostasCertas) {
		this.respostasCertas = respostasCertas;
	}

	public int getRespostasErradas() {
		return respostasErradas;
	}

	public void setRespostasErradas(int respostasErradas) {
		this.respostasErradas = respostasErradas;
	}

}
