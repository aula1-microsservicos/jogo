package com.itau.jogo.amq;

import java.util.Random;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class AmqSender {

	@Autowired private JmsTemplate jmsTemplate;

	  
	
	public ObjectMessage obtemPergunta(int qtd, String isRandom) throws JMSException{
		jmsTemplate.setReceiveTimeout(10000);
		Message msg = jmsTemplate.sendAndReceive("b.queue.questions.full", new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				MapMessage messageTosend = session.createMapMessage();
				messageTosend.setShort("quantity", (short) 9);
				messageTosend.setString("random", "yes");
                return messageTosend;

			}
		});
		System.out.println("Imprimindo mensagem jogo ->>> " + (ObjectMessage) msg);
		return  ((ObjectMessage) msg);
		
	}

	public String obterPergunta	(int qtd, String isRandom) {
		
    	Message message = jmsTemplate.sendAndReceive("b.queue.questions.full", new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                MapMessage messageTosend = session.createMapMessage();
                messageTosend.setString("quantidade", String.valueOf(qtd));
                messageTosend.setString("isRandom", isRandom);
          
                
                
                Destination tempQueue = session.createTemporaryQueue();
                
                MessageConsumer responseConsumidor = session.createConsumer(tempQueue);
               
                
                Queue queue = session.createQueue("b.queue.questions.full");
                
                //Queue queue = session.createQueue("b2.queue.questions.full");
                messageTosend.setJMSCorrelationID(new Random().nextInt(10) + "");
                messageTosend.setJMSReplyTo(tempQueue);
                
                System.out.println("tempQueue --> " + tempQueue);
                
                MessageProducer producer = session.createProducer(queue);
                producer.send(messageTosend);
                
                System.out.println("Mensagem : " + messageTosend.getJMSMessageID());
                
                MapMessage resposta = (MapMessage) responseConsumidor.receive(5000);
                //Message retorno = session.createConsumer(queue).receive();
                
                responseConsumidor.close();
                
                
                return messageTosend;
            
            };
    	});
    	
    	return message.toString();



	}


}
