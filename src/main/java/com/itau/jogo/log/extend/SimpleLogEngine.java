package com.itau.jogo.log.extend;

import com.itau.jogo.log.LogEngine;

public class SimpleLogEngine extends LogEngine{

	@Override
	public void log(String message) {
		System.out.println(message);
	}
}
