package com.itau.jogo.log.extend;

import com.itau.jogo.kafka.ProducerKafka;
import com.itau.jogo.log.LogEngine;

public class KafkaLogEngine extends LogEngine {

	@Override
	public void log(String message) {
		System.out.println("Imprime message KafkaLogEngine" + message);
		ProducerKafka.gravarNoLog(message);
	}

}
