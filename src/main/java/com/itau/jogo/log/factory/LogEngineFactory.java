package com.itau.jogo.log.factory;

import com.itau.jogo.log.LogEngine;
import com.itau.jogo.log.extend.KafkaLogEngine;
import com.itau.jogo.log.extend.SimpleLogEngine;

public abstract class LogEngineFactory {

	public static final int SIMPLE = 0;
	public static final int KAFKA = 1;

	public static LogEngine getEngine(int type) {
		switch (type) {
		case SIMPLE:
			return new SimpleLogEngine();
		case KAFKA:	
			return new KafkaLogEngine();
		default:
			return null;
		}
	}
}
