package com.itau.jogo.log;

public abstract class LogEngine {

	public void log(Event message) {
		log(message.toString());
	}
	
	public abstract void log(String message);
}
