package com.itau.jogo.log;

import com.itau.jogo.log.factory.LogEngineFactory;
import com.itau.jogo.model.Trackable;

public class Events {
	
	public static void emmit(String source, String action) {
		emmit(source, action, null);
	}
	
	public static void emmit(String source, String action, Trackable trackable) {
		Event event = new Event(source, action, trackable);
		LogEngine logger = LogEngineFactory.getEngine(LogEngineFactory.SIMPLE);
		logger.log(event);
	}
	
	public static void emmitKafka(String source, String action, Trackable trackable) {
		Event event = new Event(source, action, trackable);
		LogEngine logger = LogEngineFactory.getEngine(LogEngineFactory.KAFKA);
		logger.log(event);
	}
}
