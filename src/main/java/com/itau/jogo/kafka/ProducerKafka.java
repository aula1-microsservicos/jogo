package com.itau.jogo.kafka;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class ProducerKafka {

	private static final int P_THREADS = 16;

	private static final String TOPIC = "game";
	private static final String SERVERS = "10.162.107.229:9090;10.162.107.229:9091;10.162.107.229:9092";


	/**
	 * 
	 * @param index
	 * @return
	 */
	public static KafkaProducer<String, String> createProducer(String index) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "Produtor Talitha-" + index);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		KafkaProducer<String, String> producer = new KafkaProducer<>(props);
		return producer;
	}

	public static void gravarNoLog( String mensagem) {

		//System.out.printf("Starting.\n", Thread.currentThread().getName());
		Random random = new Random();

		KafkaProducer<String, String> producer = createProducer("b");


		final String topico = TOPIC;
		final String key = "b.log." + String.valueOf(random.nextInt(10000000));
		final String body = "Corpo da mensagem " + mensagem;

		ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);

		try {
			producer.send(record).get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		producer.flush();
		producer.close();

	}


}
