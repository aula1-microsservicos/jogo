package com.itau.jogo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@ComponentScan({"com.itau.jogo.*"})
@EnableJms
public class JogoApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(JogoApp.class, args);
    } 
}
